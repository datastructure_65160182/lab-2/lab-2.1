/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2_1;

import java.util.Arrays;

/**
 *
 * @author PT
 */
public class ArrayDeletionsLab {

    
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5};
        
        System.out.println("Original Array: " + Arrays.toString(array));
        
        // Test deleteElementByIndex
        int indexToDelete = 2;
        array = deleteElementByIndex(array, indexToDelete);
        System.out.println("Array after deleting element at index " + indexToDelete + ": " + Arrays.toString(array));
        
        // Test deleteElementByValue
        int valueToDelete = 4;
        array = deleteElementByValue(array, valueToDelete);
        System.out.println("Array after deleting element with value " + valueToDelete + ": " + Arrays.toString(array));
    }
    
    public static int[] deleteElementByIndex(int[] arr, int index) {
        if (index < 0 || index >= arr.length) {
            return arr; // No change if index is out of bounds
        }
        
        int[] updatedArray = new int[arr.length - 1];
        
        for (int i = 0, j = 0; i < arr.length; i++) {
            if (i != index) {
                updatedArray[j++] = arr[i];
            }
        }
        
        return updatedArray;
    }
    
    public static int[] deleteElementByValue(int[] arr, int value) {
        int indexToDelete = -1;
        
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                indexToDelete = i;
                break;
            }
        }
        
        if (indexToDelete == -1) {
            return arr; // No change if value is not found
        }
        
        return deleteElementByIndex(arr, indexToDelete);
    }

}

